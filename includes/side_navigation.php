<div class="well sidebar-nav">
    <ul class="nav nav-list">
        <li class="nav-header">Files / Folders
        </li>
        <li>
            <a href="/files/folder_structure.php">Folder structure</a>
        </li>
        <li>
            <a href="/files/naming_conventions.php">Naming conventions</a>
        </li>
        <li class="nav-header">PHP
        </li>
        <li>
            <a href="/php/classes.php">Classes</a>
        </li>
        <li>
            <a href="/php/functions.php">Functions</a>
        </li>
        <li>
            <a href="#">Comments</a>
        </li>
        <li class="nav-header">Javascript
        </li>
        <li>
            <a href="#">Link</a>
        </li>
        <li>
            <a href="#">Link</a>
        </li>
        <li>
            <a href="#">Link</a>
        </li>
        <li>
            <a href="#">Link</a>
        </li>
        <li>
            <a href="#">Link</a>
        </li>
        <li>
            <a href="#">Link</a>
        </li>
        <li class="nav-header">Sidebar
        </li>
        <li>
            <a href="#">Link</a>
        </li>
        <li>
            <a href="#">Link</a>
        </li>
        <li>
            <a href="#">Link</a>
        </li>
    </ul>
</div><!--/.well -->
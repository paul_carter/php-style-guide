<?php include("../includes/header.php"); ?>
            <div class="span3">
                <?php include("../includes/side_navigation.php"); ?>
            </div><!--/span-->
            <div class="span9">

                <div class="page-header">
                    <h1>Functions <small>Php</small></h1>
                </div>

                <h2 class="text-info">Opening lines</h2>

                <p>
                    A PHP function should open with a comment:
                </p>

                <pre>
/**
 * Short description of function
 *
 * Then a longer description. This would describe why this is needed and how to use the function
 * It's also handy to describe where it is to be called from.
 * If this is a revision of a previous function describe the changes
 *
 * @param array $options (parameters and their object type). One line for each parameter
 * @return int (What, if anything is returned)
 * @author Author name
 * @date Date
 * @version Function revision number
 */
</pre>

                <h2 class="text-info">The function itself</h2>

                <p>
                    The function doesn't need to be declared as public as that is implied, however private functions must be declared as private and start with a single underscore.
                </p>

                <pre>function public_function($options){ ... }</pre>

                <pre>function private _private_function($options){ ... }</pre>

                <p>
                    The function name should reflect the purpose of this function, such as create_user
                </p>

                <h2 class="text-info">Best practises</h2>

                <p>
                    Paramaters should be, where possible, a single array of options. This will allow the passing of additional parameters later during development without worrying about effecting pre-existing calls to this function.
                </p>

                <p>
                    The first curly brace should be on the line after the function declaration, not directly after the declaration.
                </p>

                <pre>function public_function($options)
{
    if(isset($options['value'])){
        return true;
    }
}
</pre>


            </div><!--/span-->
<?php include("../includes/footer.php"); ?>

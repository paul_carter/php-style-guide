<?php include("../includes/header.php"); ?>
            <div class="span3">
                <?php include("../includes/side_navigation.php"); ?>
            </div><!--/span-->
            <div class="span9">

                <div class="page-header">
                    <h1>Classes <small>Php</small></h1>
                </div>

                <h2 class="text-info">Opening lines</h2>

                <p>
                    A PHP class should open with a:
                </p>

                <pre>&lt;?php</pre>

                <p>
                    Then a comment stating the name, author, date, revision and description
                </p>

                <pre>
/***********************************************************************************************

   Name:   Class_name Class
   ---------------------------------------------------------------------------------------------
   Author: Paul Beardsell / Carter Digital
   Date: 30/10/12
   Version: 1.01
   ---------------------------------------------------------------------------------------------
   Description:
   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris.
   Vivamus hendrerit arcu sed erat molestie vehicula. Sed auctor neque eu tellus rhoncus ut
   eleifend nibh porttitor. Ut in nulla enim. Phasellus.
   molestie magna non.

************************************************************************************************/
</pre>

                <p>
                    The name should reflect the class name, the date and author of when the document was originally created and the version number should change anytime the document is commited.
                </p>

                <h2 class="text-info">The class itself</h2>

                <p>
                    Class Name should be capitalised and use underscores instead of spaces.
                </p>

                <pre>
class Class_name {
    ...
}
</pre>
                <p>
                    Classes should end in a single blank line with no closing php tag.
                </p>

            </div><!--/span-->
<?php include("../includes/footer.php"); ?>
